<?php $this->load->view('template/head', array('title' => $title)); ?>
<link href="<?= base_url(); ?>dist/css/pages/dashboard1.css" rel="stylesheet">
<!-- Date picker plugins css -->
<link href="<?= base_url(); ?>assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<!-- Daterange picker plugins css -->
<link href="<?= base_url(); ?>assets/node_modules/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="<?= base_url(); ?>assets/node_modules/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link href="<?= base_url(); ?>assets/node_modules/multiselect/css/multi-select.css" rel="stylesheet" type="text/css" />
<?php $this->load->view('template/header'); ?>
<?php $this->load->view('template/nav', array('title' => $title)); ?>

<form id="form-search" class="floating-labels">
    <div class="card">
        <div class="card-header">
            <h4>Busqueda Avanzada "Consultor"</h4>
        </div>
        <div class="card-body">
            <div class="row  m-t-10 ">
                <div class=" form-group col-md-3">
                    <input value="2007/01 - 2007/12" type="text" class="form-control input-daterange-datepicker" name="periodo" id="periodo" />
                    <span class="bar"></span>
                    <label class="text-danger" for="periodo">Periodo</label>
                </div>

                <div class=" form-group col-md-5 justify-content-center">
                    <select name="usuarios_select[]" id='usuarios_select' multiple='multiple'>
                        <?php foreach ($consultores as $value) : ?>
                            <option value="<?= $value->co_usuario . "-" . $value->no_usuario ?>"><?= $value->no_usuario ?></option>
                        <?php endforeach ?>
                    </select>
                </div>

                <div class=" form-group col-md-4">
                    <button type="button" id="relatorio" value="1" class="btn  btn-outline-primary">
                        Relatorio
                    </button>&nbsp;
                    <button type="button" id="grafico" value="2" class="btn  btn-outline-info">
                        Gráfico
                    </button>&nbsp;
                    <button type="button" id="pizza" value="3" class="btn  btn-outline-danger">
                        Pizza
                    </button>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="tipo_consulta" id="tipo_consulta" value="">
</form>

<div class="row justify-content-center">

    <!-- column -->
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Resultados</h4>
            </div>
            <div class="card-body">
                <div class="row">
                    <div id="tabla_consultor" class="col-md-12"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- column -->

</div>

<?php $this->load->view('template/footer'); ?>
<!-- Chart JS -->
<!--<script src="<?= base_url(); ?>assets/node_modules/Chart.js/chartjs.init.js?v=3"></script>-->
<script src="<?= base_url(); ?>assets/node_modules/Chart.js/Chart.min.js"></script>
<!-- Chart JS -->
<script src="<?= base_url(); ?>assets/node_modules/echarts/echarts-all.js"></script>
<!-- Plugin JavaScript -->
<script src="<?= base_url(); ?>assets/node_modules/moment/moment.js"></script>
<!-- Date range Plugin JavaScript -->
<script src="<?= base_url(); ?>assets/node_modules/timepicker/bootstrap-timepicker.min.js"></script>
<script src="<?= base_url(); ?>assets/node_modules/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/node_modules/multiselect/js/jquery.multi-select.js"></script>
<!-- custom JavaScript -->
<script src="<?= base_url(); ?>assets/js/search.js?v=2a"></script>
</body>

</html>