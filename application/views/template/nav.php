 <!-- ============================================================== -->
 <!-- Left Sidebar - style you can find in sidebar.scss  -->
 <!-- ============================================================== -->
 <div class="side-mini-panel">
     <ul class="mini-nav">
         <div class="togglediv"><a href="javascript:void(0)" id="togglebtn"><i class="ti-menu"></i></a></div>

         <li>
             <a href="javascript:void(0)"><i class="icon-check"></i></a>
             <div class="sidebarmenu">
                 <!-- Left navbar-header -->
                 <h3 class="menu-title">Comercial</h3>
                 <ul class="sidebar-menu">
                     <li><a href="<?= site_url('/') ?>">Performancer Comercial</a></li>
                 </ul>
                 <!-- Left navbar-header end -->
             </div>
         </li>
         <li>
             <a href="javascript:void(0)"><i class="icon-home"></i></a>
             <div class="sidebarmenu">
                 <!-- Left navbar-header -->
                 <h3 class="menu-title">Agence</h3>
                 <ul class="sidebar-menu">
                     <li><a href="javascript:void(0)">Ir</a></li>
                 </ul>
                 <!-- Left navbar-header end -->
             </div>
         </li>
         <li>
             <a href="javascript:void(0)"><i class="icon-book-open"></i></a>
             <div class="sidebarmenu">
                 <!-- Left navbar-header -->
                 <h3 class="menu-title">Proyectos</h3>
                 <ul class="sidebar-menu">
                     <li><a href="javascript:void(0)">Ir</a></li>
                 </ul>
                 <!-- Left navbar-header end -->
             </div>
         </li>
         <li>
             <a href="javascript:void(0)"><i class="icon-pencil"></i></a>
             <div class="sidebarmenu">
                 <!-- Left navbar-header -->
                 <h3 class="menu-title">Administrativo</h3>
                 <ul class="sidebar-menu">
                     <li><a href="javascript:void(0)">Ir</a></li>
                 </ul>
                 <!-- Left navbar-header end -->
             </div>
         </li>
        
         <li>
             <a href="javascript:void(0)"><i class="icon-wallet"></i></a>
             <div class="sidebarmenu">
                 <!-- Left navbar-header -->
                 <h3 class="menu-title">Financiero</h3>
                 <ul class="sidebar-menu">
                     <li><a href="javascript:void(0)">Ir</a></li>
                 </ul>
                 <!-- Left navbar-header end -->
             </div>
         </li>
         <li>
             <a href="javascript:void(0)"><i class="icon-user"></i></a>
             <div class="sidebarmenu">
                 <!-- Left navbar-header -->
                 <h3 class="menu-title">Usuario</h3>
                 <ul class="sidebar-menu">
                     <li><a href="javascript:void(0)">Ir</a></li>
                 </ul>
                 <!-- Left navbar-header end -->
             </div>
         </li>
         <li>
             <a href="javascript:void(0)"><i class="icon-logout"></i></a>
             <div class="sidebarmenu">
                 <!-- Left navbar-header -->
                 <h3 class="menu-title">Salir</h3>
                 <ul class="sidebar-menu">
                     <li><a href="javascript:void(0)">Ir</a></li>
                 </ul>
                 <!-- Left navbar-header end -->
             </div>
         </li>

     </ul>
 </div>
 <!-- ============================================================== -->
 <!-- End Left Sidebar - style you can find in sidebar.scss  -->
 <!-- ============================================================== -->
 <!-- ============================================================== -->
 <!-- Page wrapper  -->
 <!-- ============================================================== -->
 <div class="page-wrapper">
     <!-- ============================================================== -->
     <!-- Container fluid  -->
     <!-- ============================================================== -->
     <div class="container-fluid">
         <!-- ============================================================== -->
         <!-- Bread crumb and right sidebar toggle -->
         <!-- ============================================================== -->
         <div class="row page-titles">
             <div class="col-md-12">
                 <h4 class="text-white"><?= $title ?></h4>
             </div>
             <div class="col-md-6">
                 <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
                     <li class="breadcrumb-item active"><?= $title ?></li>
                 </ol>
             </div>
             <div class="col-md-6 text-right">
             </div>
         </div>
         <!-- ============================================================== -->
         <!-- End Bread crumb and right sidebar toggle -->
         <!-- ============================================================== -->
         <!-- ============================================================== -->
         <!-- Start Page Content -->
         <!-- ============================================================== -->