<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Errores extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        header('Content-Type: text/html');
    }

    public function index()
    { 
        $this->load->view('errors/404_view');
    }

}
