<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        header('Content-Type: text/html');
    }

    public function index()
    {
        $this->load->model('Searches_model', 'search');

        $data['consultores'] = $this->search->getConsultores(); 
        $data['title']   = 'Con Desempenho';
        
        $this->load->view('home/home_view', $data);
    }
}
