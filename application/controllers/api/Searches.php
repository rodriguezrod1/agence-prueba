<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Searches extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Searches_model', 'search');
        header('Content-Type: application/json');
    }


    
    public function getSearch()
    {
        echo json_encode($this->search->getSearch());
    }

}
