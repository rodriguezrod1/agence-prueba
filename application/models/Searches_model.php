<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Searches_model extends CI_Model
{



    // Consulta los ganancias de los consultores de acuerdo a los filtros 
    public function getSearch()
    {
        try {

            $p                  = $this->input->post('periodo', true);
            $usuarios_select    = $this->input->post('usuarios_select', true);
            $tipo_consulta      = $this->input->post('tipo_consulta', true);

            // Separa y formatea los rangos de fechas seleccionados
            $fechas = explode('-', $p);
            $desde = str_replace("/", "-", trim($fechas[0]) . "/01");
            $hasta = str_replace("/", "-", trim($fechas[1]) . "/31");

            $resultados = array();
            $labels     = array();
            $data       = array();
            $total      = 0;

            // Formulas para ser reutilizadas en los diferentes casos
            $case_costo_fijo    = "case when brut_salario is null then 0 else brut_salario end";
            $comision           = "( (valor - ( valor * (total_imp_inc/100) ) ) * (comissao_cn/100) )";
            $receita_liquida    = "(valor-(total_imp_inc/100))";

            // switch de acuedo a la consulta (Relatorio, Gráfico y Pizza)
            switch ($tipo_consulta) {
                case 1: // relatorio

                    // Itera por cada  consultor selecccionado 
                    foreach ($usuarios_select as $key => $value) {

                        // Separa el co_usuario y el nombre del selector seleccionado para usarlos apartes  
                        $sep        = explode("-", $value);
                        $id         = $sep[0];
                        $consultor  = $sep[1];

                        $this->db->select(' DISTINCT DATE_FORMAT( f.data_emissao, "%M %Y") as periodo, ');
                        $this->db->select(' ROUND(SUM(total),2) as valor, ');
                        $this->db->select(' ROUND( ' . $receita_liquida . ' ,2) as liquida, ');
                        $this->db->select($case_costo_fijo . '  as bruto_salario , ', false);
                        $this->db->select(' ROUND( ' . $comision . ' ,2) as comissao, ');
                        $this->db->select(' ROUND( ( ' . $receita_liquida . ' - ( ' . $case_costo_fijo . ' + ' . $comision . ' ) ) ,2) as lucro', false);
                        $this->db->from('cao_fatura f');
                        $this->db->join('cao_os o', 'o.co_os=f.co_os', 'left');
                        $this->db->join('cao_salario s', 's.co_usuario=o.co_usuario', 'left');
                        $this->db->where("o.co_usuario", $id);
                        $this->db->where("f.data_emissao BETWEEN '$desde' AND '$hasta'", "", FALSE);
                        $this->db->group_by('MONTH(f.data_emissao), YEAR(f.data_emissao)');
                        $this->db->order_by('valor', 'desc');
                        $row = $this->db->get()->result();

                        $resultados[$consultor] = $row;
                    }

                    break;
                case 2: // Gráfico

                    foreach ($usuarios_select as $key => $value) {

                        $sep        = explode("-", $value);
                        $id         = $sep[0];
                        $consultor  = $sep[1];

                        $this->db->select(' DISTINCT DATE_FORMAT( f.data_emissao, "%M %Y") as periodo, ');
                        $this->db->select(' ROUND(SUM(total),2) as valor, ');
                        $this->db->select(' ROUND( ' . $receita_liquida . ' ,2) as liquida, ');
                        $this->db->from('cao_fatura f');
                        $this->db->join('cao_os o', 'o.co_os=f.co_os', 'left');
                        $this->db->where("o.co_usuario", $id);
                        $this->db->where("f.data_emissao BETWEEN '$desde' AND '$hasta'", "", FALSE);
                        $this->db->group_by('MONTH(f.data_emissao), YEAR(f.data_emissao)');
                        $this->db->order_by('valor', 'desc');
                        $row = $this->db->get()->result();

                        $labels[]   = $consultor;
                        $data[]     = $row;
                    }

                    $resultados['labels']   = $labels;
                    $resultados['data']     = $data;
                    break;
                case 3: // Pizza

                    foreach ($usuarios_select as $key => $value) {

                        $sep        = explode("-", $value);
                        $id         = $sep[0];
                        $consultor  = $sep[1];

                        $this->db->select(' ROUND(SUM(total),2) as valor, ');
                        $this->db->select(' ROUND( ' . $receita_liquida . ' ,2) as liquida, ');
                        $this->db->from('cao_fatura f');
                        $this->db->join('cao_os o', 'o.co_os=f.co_os', 'left');
                        $this->db->where("o.co_usuario", $id);
                        $this->db->where("f.data_emissao BETWEEN '$desde' AND '$hasta'", "", FALSE);
                        $row = $this->db->get()->result();

                        $labels[]   = $consultor;
                        $data[]     = $row[0]->liquida;

                        $total += $row[0]->liquida;
                    }

                    // Nombres de cada consultor para mostrarlos en la gráfica 
                    $resultados['labels']   = $labels;

                    // Calcula y guarda en un arreglo los promedios de cada consultor 
                    $result = array();
                    foreach ($data as  $value) {
                        $result[] = number_format((($value / $total) * 100), 2);
                    }

                    $resultados['data'] = $result;

                    break;
                default:
                    # code...
                    break;
            }

            if (empty($resultados)) {
                throw new Exception('No se encontraron datos que mostrar.');
            }

            return [
                'status'    => 'success',
                'message'   => 'Consultado con Exito',
                'data'      => $resultados,
                'code'      => 0,
            ];
        } catch (Exception $e) {
            return [
                'status'    => 'error',
                'message'   => $e->getMessage(),
                'code'      => 0,
            ];
        }
    }


    //    Consulta los  Consultores
    public function getConsultores()
    {
        $this->db->select('c.co_usuario, c.no_usuario');
        $this->db->from('cao_usuario c');
        $this->db->join('permissao_sistema p', 'c.co_usuario=p.co_usuario', 'left');
        $this->db->where("p.co_sistema", 1);
        $this->db->where("p.in_ativo", "S");
        $tipo = "(p.co_tipo_usuario = 0 OR p.co_tipo_usuario = 1 OR p.co_tipo_usuario = 2)";
        $this->db->where($tipo, "", FALSE);
        return $this->db->get()->result();
    }
}
