<?php

//Variables para el envio de correo electrónicos

$config['useragent']    = "CodeIgniter";
$config['mailpath']     = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
$config['wordwrap']     = TRUE; //TRUE or FALSE 
$config['mailtype']     = 'html'; // text or html
$config['charset']      = 'UTF-8';
$config['newline']      = "\r\n"; // “\r\n” or “\n” or “\r”
$config['crlf']         = "\r\n"; // “\r\n” or “\n” or “\r”
$config['protocol']     = 'mail'; //mail, sendmail, or smtp
$config['smtp_host']    = '';
$config['smtp_user']    = '';
$config['smtp_pass']    = '';
$config['smtp_port']    = 465;
$config['smtp_timeout'] = 30;
$config['smtp_crypto']  = 'tls';

