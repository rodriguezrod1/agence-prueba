<?php

defined('BASEPATH') or exit('No direct script access allowed');

$CI = &get_instance();




$config['busqueda'] = [
    [
        'field'  => 'periodo',
        'label'  => 'periodo',
        'rules'  => 'trim|required',
        'errors' => [
            'required' => 'El Periodo es requerido'
        ]
    ],
    [
        'field'  => 'usuarios_select[]',
        'label'  => 'usuarios_select',
        'rules'  => 'trim|required',
        'errors' => [
            'required' => 'El Consultor es requerido'
        ]
    ]
];

