$(function() {

    // Calendario por rango de meses
    // ---------------------------------------------------------------------------------------------------
    $('.input-daterange-datepicker').daterangepicker({
        buttonClasses: ['btn', 'btn-sm'],
        applyButtonClasses: 'btn-outline-danger',
        cancelButtonClasses: 'btn-outline-inverse',
        locale: {
            format: 'YYYY/M'
        }
    });

    // Selector multiple 
    // -----------------------------------------------------------------------------------------------------
    $('#usuarios_select').multiSelect();


    // Ajax de consulta de rangos de fechas de acuerdo los consultores seleccionados y construye el Gráfico
    // -----------------------------------------------------------------------------------------------------
    $("#grafico").on('click', function(e) {
        e.preventDefault();
        var form = $('#form-search');
        var btn = $('#grafico');
        $('#tipo_consulta').val(2);
        $.ajax({
            type: "POST",
            url: `${$('#base_api').val()}api/Searches/getSearch`,
            data: form.serialize(),
            beforeSend: function() {
                $('button, input, select, textarea').attr("disabled", true);
                btn.html('Procesando...');
            },
            success: function(json) {
                $('button, input, select, textarea').attr("disabled", false);
                btn.html('Gráfico');
                if (NotificationService(json)) {
                    var data = json.data;
                    $('#tabla_consultor').html('<div id="bar-chart" style="width:100%; height:400px;"></div>');

                    // ============================================================== 
                    // Bar chart option
                    // ============================================================== 
                    var myChart = echarts.init(document.getElementById('bar-chart'));

                    // specify chart configuration item and data
                    option = {
                        tooltip: {
                            trigger: 'axis'
                        },
                        legend: {
                            data: []
                        },
                        toolbox: {
                            show: true,
                            feature: {
                                magicType: {
                                    show: true,
                                    type: ['line', 'bar']
                                },
                                restore: {
                                    show: true
                                },
                                saveAsImage: {
                                    show: true
                                }
                            }
                        },
                        //color: ["#55ce63", "#009efb", "red", "blue", "black"],
                        calculable: true,
                        xAxis: [{
                            type: 'category',
                            data: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec']
                        }],
                        yAxis: [{
                            type: 'value'
                        }],
                        series: []
                    };

                    // Agrega las leyenda de la gráfica
                    $.each(data.labels, function(key, v) {
                        option.legend.data.push(v);
                    });

                    // Itera sobre los datos para agregar los  valores de cada leyenda
                    $.each(data.data, function(key, v) {
                        // var datos = new Array();
                        option.series.push({
                            name: 'Consultor #' + key,
                            type: 'bar',
                            data: [],
                            markPoint: {
                                data: [{
                                        type: 'max',
                                        name: 'Max'
                                    },
                                    {
                                        type: 'min',
                                        name: 'Min'
                                    }
                                ]
                            },
                            markLine: {
                                data: [{
                                    type: 'average',
                                    name: 'Average'
                                }]
                            }
                        });

                        $.each(v, function(key2, v2) {
                            option.series[key].data.push(parseFloat(v2.liquida));
                        });
                    });

                    // console.log(option);

                    // use configuration item and data specified to show chart
                    myChart.setOption(option, true), $(function() {
                        function resize() {
                            setTimeout(function() {
                                myChart.resize()
                            }, 100)
                        }
                        $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
                    });

                }
            }
        });
    });


    // Ajax de consulta de rangos de fechas de acuerdo los consultores seleccionados y construye el Gráfico tipo Pizza o torta
    // -----------------------------------------------------------------------------------------------------
    $("#pizza").on('click', function(e) {
        e.preventDefault();
        var form = $('#form-search');
        var btn = $('#pizza');
        $('#tipo_consulta').val(3);
        $.ajax({
            type: "POST",
            url: `${$('#base_api').val()}api/Searches/getSearch`,
            data: form.serialize(),
            beforeSend: function() {
                $('button, input, select, textarea').attr("disabled", true);
                btn.html('Procesando...');
            },
            success: function(json) {
                $('button, input, select, textarea').attr("disabled", false);
                btn.html('Pizza');
                if (NotificationService(json)) {
                    var data = json.data;
                    $('#tabla_consultor').html('<canvas id="grafico-torta" height="120"></canvas>');

                    new Chart(document.getElementById("grafico-torta"), {
                        "type": "pie",
                        "data": {
                            "labels": data.labels,
                            "datasets": [{
                                "label": "Gráfico Tipo Pizza",
                                "data": data.data,
                                "backgroundColor": ["rgb(255, 99, 132)", "rgb(54, 162, 235)", "rgb(255, 205, 86)", "red", "green", "black"]
                            }]
                        }
                    });

                }
            }
        });
    });


    // Ajax de consulta de rangos de fechas de acuerdo los consultores seleccionados y construye Las tablas de los calculos Relatorios
    //-------------------------------------------------------------------------------------------------
    $("#relatorio").on('click', function(e) {
        e.preventDefault();
        var form = $('#form-search');
        var btn = $('#relatorio');
        $('#tipo_consulta').val(1);
        $.ajax({
            type: "POST",
            url: `${$('#base_api').val()}api/Searches/getSearch`,
            data: form.serialize(),
            beforeSend: function() {
                $('button, input, select, textarea').attr("disabled", true);
                btn.html('Procesando...');
            },
            success: function(json) {
                $('button, input, select, textarea').attr("disabled", false);
                btn.html('Relatorio');
                if (NotificationService(json)) {
                    var data = json.data,
                        table, tr, t = 0,
                        color;
                    $('#tabla_consultor').html('');

                    $.each(data, function(i, v) {

                        table = '<h4>' + i + '</h4><table class="table table-light table-bordered table-striped font-10 m-b-15">' +
                            '<thead class="thead-light">' +
                            '<tr>' +
                            '<th>Período</th>' +
                            '<th>Receita Líquida</th>' +
                            '<th>Custo Fixo</th>' +
                            '<th>Comissão</th>' +
                            '<th>Lucro</th>' +
                            '</tr>' +
                            '</thead>' +
                            '<tbody id="bodyTable' + t + '">' +
                            '</tbody>' +
                            '<tfoot class="font-weight-bold">' +
                            '<tr>' +
                            '<th>SALDO</th>' +
                            '<td id="tLiq' + t + '"></td>' +
                            '<td id="tBru' + t + '"></td>' +
                            '<td id="tCom' + t + '"></td>' +
                            '<td id="tLuc' + t + '"></td>' +
                            '</tr>' +
                            '</tfoot>' +
                            '</table>';

                        $('#tabla_consultor').append(table);

                        var t1 = 0,
                            t2 = 0,
                            t3 = 0,
                            t4 = 0;

                        $.each(v, function(j, v2) {
                            color = v2.lucro >= 0 ? "text-success" : "text-danger";
                            t1 += parseFloat(v2.liquida);
                            t2 += parseFloat(v2.bruto_salario);
                            t3 += parseFloat(v2.comissao);
                            t4 += parseFloat(v2.lucro);
                            tr = '<tr>' +
                                '<td>' + v2.periodo + '</td>' +
                                '<td>R$ ' + formato_numero(v2.liquida, 2, ",", ".") + '</td>' +
                                '<td>R$ ' + formato_numero(v2.bruto_salario, 2, ",", ".") + '</td>' +
                                '<td>R$ ' + formato_numero(v2.comissao, 2, ",", ".") + '</td>' +
                                '<td class="' + color + '">R$ ' + formato_numero(v2.lucro, 2, ",", ".") + '</td>' +
                                '</tr>';
                            $('#bodyTable' + t).append(tr);
                        });

                        $('#tLiq' + t).html(formato_numero(t1, 2, ",", "."));
                        $('#tBru' + t).html(formato_numero(t2, 2, ",", "."));
                        $('#tCom' + t).html(formato_numero(t3, 2, ",", "."));
                        $('#tLuc' + t).html(formato_numero(t4, 2, ",", "."));

                        t += 1;
                    });

                }
            }
        });
    });

});