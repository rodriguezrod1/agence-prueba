function NotificationService(obj) {
    switch (obj.code) {
        //                    Errores de Validación
        case 800:
            var msj = [];
            $.each(obj.data, function(i, v) {
                msj.push(v + "<br>");
            });
            toast(msj, 'error');
            return false;
            break;
            //                        Error de fin de sesión
        case 900:
            toast('Su Sesión a Finalizado, por favor vuelva a Ingresar.', 'warning');
            setTimeout(function() {
                location = `${$('#base_api').val()}Logout/`;
            }, 2000);
            break;
            //                        Respuesta del método que procesa error / success
        default:
            if (obj.status === 'error') {
                toast(obj.message, 'error');
                return false;
            }
            toast(obj.message, 'success');
            return true;
            break;
    }
}

function toast(mens, alert) {
    var title;
    switch (alert) {
        case 'info':
            title = '¡ Importante !';
            break;
        case 'warning':
            title = '¡ Precaución !';
            break;
        case 'success':
            title = '¡ Listo !';
            break;
        default:
            title = '¡ Disculpe !';
            break;
    }

    $.toast({
        heading: title,
        text: mens,
        position: 'top-right',
        loaderBg: '#ff6849',
        icon: alert,
        hideAfter: 6000,
        showHideTransition: 'slide',
        stack: 6
    });
}

function formato_numero(numero, decimales, separador_decimal, separador_miles) {
    numero = parseFloat(numero);
    if (isNaN(numero)) {
        return "";
    }

    if (decimales !== undefined) {
        // Redondeamos
        numero = numero.toFixed(decimales);
    }

    // Convertimos el punto en separador_decimal
    numero = numero.toString().replace(".", separador_decimal !== undefined ? separador_decimal : ",");

    if (separador_miles) {
        // Añadimos los separadores de miles
        var miles = new RegExp("(-?[0-9]+)([0-9]{3})");
        while (miles.test(numero)) {
            numero = numero.replace(miles, "$1" + separador_miles + "$2");
        }
    }

    return numero;
}


$(document).ready(function() {

    $.ajaxSetup({
        error: function(jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
            $('button, input, select, textarea').attr("disabled", false);
        }
    });


});